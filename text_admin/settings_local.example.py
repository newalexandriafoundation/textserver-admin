# environment dependent settings
import os

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ['DEBUG'] if 'DEBUG' in os.environ else False,

# database config
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DATABASE_NAME'] if 'DATABASE_NAME' in os.environ else 'NAME_OF_YOUR_DB',
        'USER': os.environ['DATABASE_USER'] if 'DATABASE_USER' in os.environ else 'USER_OF_YOUR_DB',
        'PASSWORD': os.environ['DATABASE_PASSWORD'] if 'DATABASE_PASSWORD' in os.environ else 'PASSWORD_OF_YOUR_DB',
        'HOST': os.environ['DATABASE_HOST'] if 'DATABASE_HOST' in os.environ else '127.0.0.1',
    }
}
